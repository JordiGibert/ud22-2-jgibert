package dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cliente {
	private int id, DNI;
	private String nombre, apellido, direccion, fecha;
	public Cliente(int id, int dNI, String nombre, String apellido, String direccion) {
		super();
		this.id = id;
		DNI = dNI;
		this.nombre = nombre;
		this.apellido = apellido;
		this.direccion = direccion;
		
		Date date = new Date();
 		DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd");
 		this.fecha=hourdateFormat.format(date);
	}
	public int getId() {
		return id;
	}
	public int getDNI() {
		return DNI;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setDNI(int dNI) {
		DNI = dNI;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	

}
