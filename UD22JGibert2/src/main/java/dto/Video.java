package dto;

public class Video {
	private int id, idCliente;
	private String titulo, director;
	public Video(int id, int idCliente, String titulo, String director) {
		super();
		this.id = id;
		this.idCliente = idCliente;
		this.titulo = titulo;
		this.director = director;
	}
	public int getId() {
		return id;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public String getTitulo() {
		return titulo;
	}
	public String getDirector() {
		return director;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	
	
	
	

}
