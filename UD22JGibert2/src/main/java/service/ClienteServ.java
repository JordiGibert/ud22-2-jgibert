package service;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import controler.ClientControler;
import dao.ClienteDao;
import dao.VideoDao;
import dto.Cliente;
import dto.Video;



public class ClienteServ {
	private ClientControler clienteController;
	public static boolean consultaCliente=false;
	public static boolean modificaCliente=false;
	public static boolean consultaVideo=false;
	public static boolean modificaVideo=false;
	
	//Metodo de vinculación con el controller principal
	public void setpersonaController(ClientControler personaController) {
		this.setControler(personaController);		
	}
	
	

	//Metodo que valida los datos de Registro antes de pasar estos al DAO
	public void validarRegistro(Cliente miCliente) {
		ClienteDao miClienteDao = new ClienteDao();
		miClienteDao.registrarCliente(miCliente);						
		
		
	}
	
	public void validarRegistroVideo(Video miVideo) {
		VideoDao miVideoDao = new VideoDao();
		miVideoDao.registrarVideo(miVideo);						
		
		
	}
	public Cliente validarConsulta(String codigoCliente) {
		ClienteDao miPersonaDao;
		
		try {
			int codigo=Integer.parseInt(codigoCliente);	
				miPersonaDao = new ClienteDao();
				consultaCliente=true;
				return miPersonaDao.buscarCliente(codigo);						
			
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultaCliente=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaCliente=false;
		}
					
		return null;
	}
	
	public Video validarConsultaVideo(String codigoVideo) {
		VideoDao miVideoDao;
		
		try {
			int codigo=Integer.parseInt(codigoVideo);	
				miVideoDao = new VideoDao();
				consultaVideo=true;
				return miVideoDao.buscarVideo(codigo);						
			
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultaVideo=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaVideo=false;
		}
					
		return null;
	}

	//Metodo que valida los datos de Modificación antes de pasar estos al DAO
	public void validarModificacion(Cliente miCliente) {
		ClienteDao miPersonaDao;
		miPersonaDao = new ClienteDao();
		miPersonaDao.modificarCliente(miCliente);	
		modificaCliente=true;
	}
	
	public void validarModificacionVideo(Video miVideo) {
		VideoDao miVideoDao;
		miVideoDao = new VideoDao();
		miVideoDao.modificarVideo(miVideo);	
		modificaVideo=true;
	}

	//Metodo que valida los datos de Eliminación antes de pasar estos al DAO
	public void validarEliminacion(String codigo) {
		ClienteDao miPersonaDao=new ClienteDao();
		miPersonaDao.eliminarCliente(codigo);
	}
	
	public void validarEliminacionVideo(String codigo) {
		VideoDao miPersonaDao=new VideoDao();
		miPersonaDao.eliminarVideo(codigo);
	}
	
	public ArrayList<Integer> pasarClientes() {
		VideoDao videoDao=new VideoDao();
		return videoDao.listarClientes();
	}
	
	public ClientControler getClientControler() {
		return clienteController;
	}

	public void setControler(ClientControler clientController) {
		this.clienteController = clientController;
	}
}
