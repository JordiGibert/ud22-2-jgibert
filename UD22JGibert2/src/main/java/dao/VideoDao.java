package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import conexion.Conexion;
import dto.Video;



public class VideoDao {
	public void registrarVideo(Video miVideo)
	{
		Conexion conex= new Conexion();
		
		try {
			Statement st = conex.getConnection().createStatement();
			String sql= "INSERT INTO video VALUES ('"+miVideo.getId()+"', '"
					+miVideo.getTitulo()+"', '"+miVideo.getDirector()+"', NULL);";
			if(miVideo.getIdCliente()!=-1) {
				sql= "INSERT INTO video VALUES ('"+miVideo.getId()+"', '"
						+miVideo.getTitulo()+"', '"+miVideo.getDirector()+"', '"
						+miVideo.getIdCliente()+"');";
			} 
			st.executeUpdate(sql);
			
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);
			st.close();
			conex.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Registro");
		}
	}


public Video buscarVideo(int codigo) 
{
	Conexion conex= new Conexion();
	Video video= new Video(0,0,"","");
	boolean existe=false;
	try {
		String sql= "SELECT * FROM video where id = ? ";
		PreparedStatement consulta = conex.getConnection().prepareStatement(sql);
		consulta.setInt(1, codigo);
		ResultSet res = consulta.executeQuery();
		while(res.next()){
			existe=true;
			video.setId(Integer.parseInt(res.getString("id")));
			video.setTitulo(res.getString("titulo"));
			video.setDirector(res.getString("director"));
			if(res.getString("cliente")==null) {
				video.setIdCliente(-1);
			} else {
				video.setIdCliente(Integer.parseInt(res.getString("cliente")));
			}
			
		 }
		res.close();
		conex.desconectar();
		System.out.println(sql);
				
		} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Error, no se conecto");
				System.out.println(e);
		}
	
		if (existe) {
			return video;
		}
		else return null;				
}

public void modificarVideo(Video miVideo) {
	
	Conexion conex= new Conexion();
	try{
		String consulta="UPDATE video SET id= ? ,titulo = ? , director=? , cliente= ? WHERE id= ? ";
		PreparedStatement estatuto = conex.getConnection().prepareStatement(consulta);
		
        estatuto.setInt(1, miVideo.getId());
        estatuto.setString(2, miVideo.getTitulo());
        estatuto.setString(3, miVideo.getDirector());
        estatuto.setInt(4,miVideo.getIdCliente());
        estatuto.setInt(5,miVideo.getId());
        estatuto.executeUpdate();
        
      JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ","Confirmación",JOptionPane.INFORMATION_MESSAGE);
      System.out.println(consulta);
     

    }catch(SQLException	 e){

        System.out.println(e);
        JOptionPane.showMessageDialog(null, "Error al Modificar","Error",JOptionPane.ERROR_MESSAGE);

    }
}

public void eliminarVideo(String codigo)
{
	Conexion conex= new Conexion();
	try {
		String sql= "DELETE FROM video WHERE id='"+codigo+"'";
		Statement st = conex.getConnection().createStatement();
		st.executeUpdate(sql);
        JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
        System.out.println(sql);
		st.close();
		conex.desconectar();
		
	} catch (SQLException e) {
        System.out.println(e.getMessage());
		JOptionPane.showMessageDialog(null, "No se Elimino");
	}
}

public ArrayList<Integer> listarClientes() {
	Conexion conex= new Conexion();
	ArrayList<Integer> clientes=new ArrayList<Integer>();
	try {
		String sql= "SELECT id FROM cliente";
		PreparedStatement consulta = conex.getConnection().prepareStatement(sql);
		ResultSet res = consulta.executeQuery();
		while(res.next()){
			clientes.add(Integer.parseInt(res.getString("id")));
		 }
		res.close();
		conex.desconectar();
		System.out.println(sql);
				
		} catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Error, no se conecto");
				System.out.println(e);
		}
	return clientes;
	
}
}
