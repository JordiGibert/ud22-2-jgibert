package controler;



import java.util.ArrayList;

import dao.VideoDao;
import dto.Cliente;
import dto.Video;
import service.ClienteServ;
import view.VentanaBuscar;
import view.VentanaBuscarVideo;
import view.VentanaPrincipal;
import view.vista;
import view.vistaVideo;

public class ClientControler {
	private ClienteServ clienteServ;
	private VentanaBuscar miVentanaBuscar;
	private VentanaBuscarVideo miVentanaBuscarVideo;
	private VentanaPrincipal miVentanaPrincipal;
	private vista vista;
	private vistaVideo vistaVideo;
	
	//Metodos getter Setters de vistas


	public vista getvista() {
		return vista;
	}
	public void setvista(vista vista) {
		this.vista = vista;
	}
	
	public vistaVideo getvistaVideo() {
		return vistaVideo;
	}
	public void setvistaVideo(vistaVideo vistaVideo) {
		this.vistaVideo = vistaVideo;
	}
	
	public ClienteServ getClienteServ() {
		return clienteServ;
	}
	public void setClienteServ(ClienteServ clienteServ) {
		this.clienteServ = clienteServ;
	}
	
	public VentanaBuscar getMiVentanaBuscar() {
		return miVentanaBuscar;
	}
	public void setMiVentanaBuscar(VentanaBuscar miVentanaBuscar) {
		this.miVentanaBuscar = miVentanaBuscar;
	}
	
	public void setMiVentanaPrincipal(VentanaPrincipal miVentanaPrincipal) {
		this.miVentanaPrincipal = miVentanaPrincipal;
	}
	
	public void setMiVentanaBuscarVideo(VentanaBuscarVideo miVentanaBuscarVideo) {
		this.miVentanaBuscarVideo = miVentanaBuscarVideo;
	}
	
	//Hace visible las vistas de Registro y Consulta
	public void mostrarvista() {
		vista.setVisible(true);
		miVentanaPrincipal.setVisible(false);
	}
	public void mostrarvistaVideo() {
		vistaVideo.resetLlista();
		vistaVideo.setVisible(true);
		miVentanaPrincipal.setVisible(false);
	}
	public void mostrarVentanaConsulta() {
		miVentanaBuscar.setVisible(true);
		miVentanaPrincipal.setVisible(false);
	}
	public void mostrarVentanaConsultaVideo() {
		miVentanaBuscarVideo.setVisible(true);
		miVentanaPrincipal.setVisible(false);
	}
	public void mostrarVentanaPrincipal() {
		miVentanaPrincipal.setVisible(true);
	}
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarCliente(Cliente miCliente) {
		clienteServ.validarRegistro(miCliente);
	}
	
	public Cliente buscarCliente(String codigoCliente) {
		return clienteServ.validarConsulta(codigoCliente);
	}
	
	public void modificarCliente(Cliente miCliente) {
		clienteServ.validarModificacion(miCliente);
	}
	
	public void eliminarCliente(String codigo) {
		clienteServ.validarEliminacion(codigo);
	}
	
	public void registrarVideo(Video miVideo) {
		clienteServ.validarRegistroVideo(miVideo);
	}
	
	public Video buscarVideo(String codigoVideo) {
		return clienteServ.validarConsultaVideo(codigoVideo);
	}
	
	public void modificarVideo(Video miVideo) {
		clienteServ.validarModificacionVideo(miVideo);
	}
	
	public void eliminarVideo(String codigo) {
		clienteServ.validarEliminacionVideo(codigo);
	}
	
	public ArrayList<Integer> pasarClientes() {
		VideoDao videoDao=new VideoDao();
		return videoDao.listarClientes();
	}
	


}
