package com.UD22.JGibert.UD22JGibert;



import controler.ClientControler;
import service.ClienteServ;
import view.VentanaBuscar;
import view.VentanaBuscarVideo;
import view.VentanaPrincipal;
import view.vista;
import view.vistaVideo;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	/*Se instancian las clases*/
    	vista vista=new vista();
    	vistaVideo vistaVideo=new vistaVideo();
    	ClienteServ clienteServ=new ClienteServ();
    	VentanaBuscar miVentanaBuscar = new VentanaBuscar();
    	VentanaBuscarVideo miVentanaBuscarVideo = new VentanaBuscarVideo();
		ClientControler clientControler=new ClientControler();
		VentanaPrincipal miVentanaPrincipal=new VentanaPrincipal();
		
		/*Se establecen las relaciones entre clases*/
		vista.setCoordinador(clientControler);
		clienteServ.setControler(clientControler);
		miVentanaBuscar.setCoordinador(clientControler);
		miVentanaBuscarVideo.setCoordinador(clientControler);
		miVentanaPrincipal.setCoordinador(clientControler);
		vistaVideo.setCoordinador(clientControler);
		
		/*Se establecen relaciones con la clase coordinador*/
		clientControler.setvista(vista);
		clientControler.setvistaVideo(vistaVideo);
		clientControler.setClienteServ(clienteServ);
		clientControler.setMiVentanaBuscar(miVentanaBuscar);
		clientControler.setMiVentanaBuscarVideo(miVentanaBuscarVideo);
		clientControler.setMiVentanaPrincipal(miVentanaPrincipal);
		
				
		miVentanaPrincipal.setVisible(true);
		
    }
}
