package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controler.ClientControler;
import dto.Cliente;
import dto.Video;
import service.ClienteServ;


public class VentanaBuscarVideo  extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private ClientControler clientController; //objeto personaController que permite la relacion entre esta clase y la clase personaController
	private JLabel labelTitulo;
	private JTextField textCod,textTitulo,textDirector;
	private JLabel cod,nombre,lblDNI;
	private JButton botonGuardar,botonCancelar,botonBuscar,botonModificar,botonEliminar;
	private JLabel lblNewLabel;
	private JLabel textCliente;
	
	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana de busqueda
	 */
	public VentanaBuscarVideo() {

		botonGuardar = new JButton();
		botonGuardar.setBounds(53, 156, 120, 25);
		botonGuardar.setText("Guardar");
		
		botonCancelar = new JButton();
		botonCancelar.setBounds(190, 192, 120, 25);
		botonCancelar.setText("Cancelar");
		
		botonBuscar = new JButton();
		botonBuscar.setBounds(170, 80, 50, 25);
		botonBuscar.setText("Ok");
		
		botonEliminar = new JButton();
		botonEliminar.setBounds(320, 156, 120, 25);
		botonEliminar.setText("Eliminar");
		
		botonModificar = new JButton();
		botonModificar.setBounds(190, 156, 120, 25);
		botonModificar.setText("Modificar");

		labelTitulo = new JLabel();
		labelTitulo.setText("ADMINISTRAR VIDEOS");
		labelTitulo.setBounds(120, 20, 380, 30);
		labelTitulo.setFont(new java.awt.Font("Verdana", 1, 18));

		cod=new JLabel();
		cod.setText("Codigo");
		cod.setBounds(20, 80, 80, 25);
		getContentPane().add(cod);
		
		nombre=new JLabel();
		nombre.setText("Titulo");
		nombre.setBounds(20, 120, 80, 25);
		getContentPane().add(nombre);
		
		lblDNI=new JLabel();
		lblDNI.setText("Director");
		lblDNI.setBounds(290, 120, 80, 25);
		getContentPane().add(lblDNI);
		
		textCod=new JTextField();
		textCod.setBounds(80, 80, 80, 25);
		getContentPane().add(textCod);
		
		textTitulo=new JTextField();
		textTitulo.setBounds(80, 120, 190, 25);
		getContentPane().add(textTitulo);
		
		textDirector=new JTextField();
		textDirector.setBounds(340, 120, 80, 25);
		getContentPane().add(textDirector);
		textCliente = new JLabel("");
		textCliente.setBounds(320, 85, 130, 14);
		getContentPane().add(textCliente);
		
		botonModificar.addActionListener(this);
		botonEliminar.addActionListener(this);
		botonBuscar.addActionListener(this);
		botonGuardar.addActionListener(this);
		botonCancelar.addActionListener(this);

		getContentPane().add(botonCancelar);
		getContentPane().add(botonBuscar);
		getContentPane().add(botonModificar);
		getContentPane().add(botonEliminar);
		getContentPane().add(botonGuardar);
		getContentPane().add(labelTitulo);
		limpiar();
				
		setSize(480, 261);
		setTitle("Patron de Diseño/MVC");
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
		
		lblNewLabel = new JLabel("Cliente id:");
		lblNewLabel.setBounds(230, 85, 80, 14);
		getContentPane().add(lblNewLabel);
		
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	
		    	setVisible(false);
		    	clientController.mostrarVentanaPrincipal();
		    	dispose();
		    }
		});

	}


	public void setCoordinador(ClientControler clientControler) {
		this.clientController=clientControler;
	}

	

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource()==botonGuardar)
		{
			try {
				int id=Integer.parseInt(textCod.getText());
				int cliente=Integer.parseInt(textCliente.getText());
				String titulo=textTitulo.getText();
				String dir=textDirector.getText();
				Video miVideo=new Video(id,cliente,titulo,dir);
				

				clientController.modificarVideo(miVideo);
				
				if (ClienteServ.modificaVideo==true) {
					habilita(true, false, false, false, false, true, false, true, true);	
				}
			} catch (Exception e2) {
				JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
			}
			
		}
		
		if (e.getSource()==botonBuscar)
		{
			Video miVideo=clientController.buscarVideo(textCod.getText());
			if (miVideo!=null)
			{
				muestraVideo(miVideo);
			}
			else if(ClienteServ.consultaVideo==true){
				JOptionPane.showMessageDialog(null, "El cliente no Existe","Advertencia",JOptionPane.WARNING_MESSAGE);
			}
		}
		
		if (e.getSource()==botonModificar)
		{
			habilita(false, true, true, true, true, false, true, false, false);
			
		}
		
		if (e.getSource()==botonEliminar)
		{
			if (!textCod.getText().equals(""))
			{
				int respuesta = JOptionPane.showConfirmDialog(this,
						"Esta seguro de eliminar el Video?", "Confirmación",
						JOptionPane.YES_NO_OPTION);
				if (respuesta == JOptionPane.YES_NO_OPTION)
				{
					clientController.eliminarVideo(textCod.getText());
					limpiar();
				}
			}
			else{
				JOptionPane.showMessageDialog(null, "Ingrese un numero de Documento", "Información",JOptionPane.WARNING_MESSAGE);
			}
			
		}
		if (e.getSource()==botonCancelar)
		{
			setVisible(false);
	    	clientController.mostrarVentanaPrincipal();
			this.dispose();
		}
		
		

	}



	/**
	 * permite cargar los datos de la persona consultada
	 * @param miVideo
	 */
	private void muestraVideo(Video miVideo) {
		textTitulo.setText(miVideo.getTitulo());
		textDirector.setText(miVideo.getDirector());
		textCliente.setText(""+miVideo.getIdCliente());
		habilita(true, false, false, false, false, true, false, true, true);
	}


	/**
	 * Permite limpiar los componentes
	 */
	public void limpiar()
	{
		textCod.setText("");
		textTitulo.setText("");
		textDirector.setText("");
		textCliente.setText("");
		habilita(true, false, false, false, false, true, false, true, true);
	}


	/**
	 * Permite habilitar los componentes para establecer una modificacion
	 * @param codigo
	 * @param nombre
	 * @param edad
	 * @param tel
	 * @param profesion
	 * @param cargo
	 * @param bBuscar
	 * @param bGuardar
	 * @param bModificar
	 * @param bEliminar
	 */
	public void habilita(boolean codigo, boolean nombre, boolean edad, boolean tel, boolean profesion,	 boolean bBuscar, boolean bGuardar, boolean bModificar, boolean bEliminar)
	{
		textCod.setEditable(codigo);
		textTitulo.setEditable(nombre);
		textDirector.setEditable(edad);
		botonBuscar.setEnabled(bBuscar);
		botonGuardar.setEnabled(bGuardar);
		botonModificar.setEnabled(bModificar);
		botonEliminar.setEnabled(bEliminar);
	}
}
