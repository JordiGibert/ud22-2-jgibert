package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import controler.ClientControler;


public class VentanaPrincipal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	private ClientControler clientControler; //objeto PersonaController que permite la relacion entre esta clase y la clase PersonaController
	private JTextArea areaIntroduccion;
	private JLabel labelTitulo, labelSeleccion;
	private JButton botonRegistrar,botonBuscar;
	private JButton btnRegistrarVideo = new JButton();
	

	/**
	 * Establece la informacion que se presentara como introduccion del sistema
	 */
	public String textoIntroduccion = "";

	/**
	 * constructor de la clase donde se inicializan todos los componentes
	 * de la ventana principal
	 */
	public VentanaPrincipal() {
		setTitle("Vitsa Controlador Cliente/Video");
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		botonRegistrar = new JButton();
		botonRegistrar.setBounds(94, 189, 120, 25);
		botonRegistrar.setText("Registrar cliente");
		
		botonBuscar = new JButton();
		botonBuscar.setBounds(224, 189, 120, 25);
		botonBuscar.setText("Buscar cliente");

		labelTitulo = new JLabel();
		labelTitulo.setText("PATRON MODELO VISTA CONTROLADOR");
		labelTitulo.setBounds(60, 40, 380, 30);
		labelTitulo.setFont(new java.awt.Font("Verdana", 1, 15));

		labelSeleccion = new JLabel();
		labelSeleccion.setText("Escoja que operacion desea realizar");
		labelSeleccion.setBounds(71, 153, 250, 25);

		textoIntroduccion = "Esta aplicación presenta un ejemplo práctico del patron "
				+ "de diseño MVC.\n\n"
				+ "La aplicación permite registrar, actualizar, buscar y eliminar registros de una tabla Persona." +
				"tambien son vinculados algunos conceptos de los Patrones Value Object y Data Access Objetc\n";

		areaIntroduccion = new JTextArea();
		areaIntroduccion.setBounds(50, 90, 380, 52);
		areaIntroduccion.setEditable(false);
		areaIntroduccion.setFont(new java.awt.Font("Verdana", 0, 14));
		areaIntroduccion.setLineWrap(true);
		areaIntroduccion.setText("Esta aplicacion muestra un modelo vista controlador de la tabla cliente y video.");
		areaIntroduccion.setWrapStyleWord(true);
		areaIntroduccion.setBorder(javax.swing.BorderFactory.createBevelBorder(
				javax.swing.border.BevelBorder.LOWERED, null, null, null,
				new java.awt.Color(0, 0, 0)));

		botonRegistrar.addActionListener(this);
		botonBuscar.addActionListener(this);
		getContentPane().add(botonBuscar);
		getContentPane().add(botonRegistrar);
		getContentPane().add(labelSeleccion);
		getContentPane().add(labelTitulo);
		getContentPane().add(areaIntroduccion);
	
		setSize(480, 297);
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);
		btnRegistrarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clientControler.mostrarvistaVideo();
			}
		});
		
		
		
		btnRegistrarVideo.setText("Registrar video");
		btnRegistrarVideo.setBounds(94, 225, 120, 25);
		getContentPane().add(btnRegistrarVideo);
		
		JButton btnBuscarVideo = new JButton();
		btnBuscarVideo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clientControler.mostrarVentanaConsultaVideo();
			}
		});
		btnBuscarVideo.setText("Buscar video");
		btnBuscarVideo.setBounds(224, 225, 120, 25);
		getContentPane().add(btnBuscarVideo);

	}


	public void setCoordinador(ClientControler clientControler) {
		this.clientControler=clientControler;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==botonRegistrar) {
			clientControler.mostrarvista();			
		}
		if (e.getSource()==botonBuscar) {
			clientControler.mostrarVentanaConsulta();			
		}
		if (e.getSource()==btnRegistrarVideo) {
			clientControler.mostrarvistaVideo();
		}
	}
}
