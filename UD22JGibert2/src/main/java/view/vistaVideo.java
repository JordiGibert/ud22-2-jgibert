package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controler.ClientControler;
import dto.Cliente;
import dto.Video;
import service.ClienteServ;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JList;
import javax.swing.ListSelectionModel;

public class vistaVideo extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textTitulo;
	private JTextField textDirector;
	private ClientControler clientControler; //objeto personaController que permite la relacion entre esta clase y la clase PersonaController
	DefaultListModel<Integer> model = new DefaultListModel<>();
	private JList<Integer> list = new JList<>(model);

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public void setCoordinador(ClientControler clientControler) {
		this.clientControler=clientControler;
	} 
	
	public void resetLlista() {
		ArrayList<Integer> listaClientes = clientControler.pasarClientes();
		for(int i=0;i<listaClientes.size();i++) {
			model.add(i, listaClientes.get(i));
		}
	}
	
	public vistaVideo() {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setTitle("Añadir cliente");
		setBounds(100, 100, 277, 390);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ID:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(78, 18, 86, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(78, 43, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Titulo:");
		lblNewLabel_1.setBounds(10, 74, 65, 14);
		contentPane.add(lblNewLabel_1);
		
		textTitulo = new JTextField();
		textTitulo.setBounds(10, 99, 86, 20);
		contentPane.add(textTitulo);
		textTitulo.setColumns(10);
		
		JLabel lblNewLabel_1_1 = new JLabel("Director:");
		lblNewLabel_1_1.setBounds(160, 74, 46, 14);
		contentPane.add(lblNewLabel_1_1);
		
		textDirector = new JTextField();
		textDirector.setColumns(10);
		textDirector.setBounds(160, 99, 86, 20);
		contentPane.add(textDirector);
		
		JButton btn = new JButton("Enviar datos");
		list.setBounds(78, 155, 86, 155);
		contentPane.add(list);
		
		//--------------------------------------------------------------------------------------------------------------
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					
					int id=Integer.parseInt(textField.getText());
					int index= list.getSelectedIndex();
					int cliente=model.get(index);
					if(index==-1) {
						cliente=-1;
					}
					
					String titulo=textTitulo.getText();
					String director=textDirector.getText();
					Video video = new Video(id,cliente,titulo,director);
					clientControler.registrarVideo(video);	
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
					System.out.println(ex);
				}
				
			}
		});
		btn.setBounds(78, 317, 104, 23);
		contentPane.add(btn);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		
	
		
		JLabel lblIdCliente = new JLabel("ID cliente:");
		lblIdCliente.setHorizontalAlignment(SwingConstants.CENTER);
		lblIdCliente.setBounds(78, 130, 86, 14);
		contentPane.add(lblIdCliente);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	
		    	setVisible(false);
		        clientControler.mostrarVentanaPrincipal();
		        dispose();
		    }
		});
		
	}
}
